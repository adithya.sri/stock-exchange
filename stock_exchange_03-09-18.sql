# ************************************************************
# Sequel Pro SQL dump
# Version 4541
#
# http://www.sequelpro.com/
# https://github.com/sequelpro/sequelpro
#
# Host: 127.0.0.1 (MySQL 5.7.22)
# Database: stock_exchange
# Generation Time: 2018-09-03 11:50:30 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Dump of table stock_data
# ------------------------------------------------------------

DROP TABLE IF EXISTS `stock_data`;

CREATE TABLE `stock_data` (
  `stock_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `CompanyID` varchar(2) DEFAULT NULL,
  `Countries` text,
  `Category` text,
  `Budget` double DEFAULT NULL,
  `Bid` double DEFAULT NULL,
  PRIMARY KEY (`stock_id`),
  UNIQUE KEY `CompanyID` (`CompanyID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `stock_data` WRITE;
/*!40000 ALTER TABLE `stock_data` DISABLE KEYS */;

INSERT INTO `stock_data` (`stock_id`, `CompanyID`, `Countries`, `Category`, `Budget`, `Bid`)
VALUES
	(1,'C1','US, FR','Automobile, Finance',1,0.1),
	(2,'C2','IN, US','Finance, IT',0.7999999999999998,0.3),
	(3,'C3','US, RU','IT, Automobile',3,0.05);

/*!40000 ALTER TABLE `stock_data` ENABLE KEYS */;
UNLOCK TABLES;



/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
