* Do a `npm install` to install the dependencies
* Run using `MYSQL_USER=root MYSQL_PASS= MYSQL_HOST=127.0.0.1 MYSQL_DB=stock_exchange node index.js` after changing the credentials
* Use stock_exchange_03-09-18.sql as the data source in the table
* Uses winston to log transactions into file (Requires a transaction ID to uniquely identify which isn't implemented)
* Visit `http://localhost/countrycode=US&Category=Automobile&BaseBid=10` (based on your port settings, this may differ)
* /logs/transactions.log has the log information